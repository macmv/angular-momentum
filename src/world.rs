use crate::pos::Pos;

pub struct World {
  pub origin:          Pos,
  pub angular_objects: Vec<AngularObject>,
  pub linear_objects:  Vec<LinearObject>,
}

pub struct AngularObject {
  /// Radius of the object on the swing.
  pub distance: f64,
  /// Angle from +X in radians.
  pub angle:    f64,
  /// Mass in kg.
  pub mass:     f64,
  /// Angular velocity in m/s.
  pub velocity: f64,

  /// Radius of the sphere.
  pub radius:                   f64,
  /// The moment of inertia of the object.
  pub moment_of_inertia:        f64,
  /// The moment of inertia of the object at the origin.
  pub moment_of_inertia_origin: f64,
}

pub struct LinearObject {
  /// Position in the world.
  pub pos:      Pos,
  /// Mass in kg.
  pub mass:     f64,
  /// Linear velocity in m/s.
  pub velocity: Pos,
}

impl AngularObject {
  pub fn new_sphere(pos: Pos, radius: f64) -> AngularObject {
    // Each sphere weighs 1 kg.
    let mass = 1.0;

    let distance = (pos.x * pos.x + pos.y * pos.y).sqrt();
    let angle = pos.y.atan2(pos.x);

    let mut object = AngularObject {
      distance,
      angle,
      mass,
      velocity: 0.0,
      radius,
      moment_of_inertia: 0.0,
      moment_of_inertia_origin: 0.0,
    };
    object.calculate_moment_of_inertia();
    object
  }

  fn calculate_moment_of_inertia(&mut self) {
    // Moment of inertia of a sphere, I = 2/5 * m * r^2.
    self.moment_of_inertia = 2.0 / 5.0 * self.mass * self.radius * self.radius;
    // Parallel axis theorem, I = Icm + m * d^2.
    self.moment_of_inertia_origin =
      self.moment_of_inertia + self.mass * self.distance * self.distance;
  }

  pub fn position(&self) -> Pos {
    Pos { x: self.distance * self.angle.cos(), y: self.distance * self.angle.sin() }
  }
}

impl LinearObject {
  pub fn new(pos: Pos, mass: f64) -> LinearObject {
    LinearObject { pos, mass, velocity: Pos::ZERO }
  }
}

impl World {
  pub fn new() -> World {
    World {
      origin:          Pos { x: 20.0, y: 5.0 },
      angular_objects: vec![],
      linear_objects:  vec![],
    }
  }

  pub fn add_angular(&mut self, object: AngularObject) { self.angular_objects.push(object); }
  pub fn add_linear(&mut self, object: LinearObject) { self.linear_objects.push(object); }

  pub fn update(&mut self, dt: f64) {
    // Merge objects that are intersecting. We only merge two objects per update,
    // because I'm lazy.
    let mut intersection = None;
    'outer: for (i, object) in self.angular_objects.iter().enumerate() {
      for (j, other) in self.angular_objects.iter().enumerate() {
        if j >= i {
          break;
        }

        let distance = (object.position() - other.position()).length();
        if distance < object.radius + other.radius {
          intersection = Some((i, j));
          break 'outer;
        }
      }
    }

    // Work backward to avoid breaking indexes
    if let Some((b_index, a_index)) = intersection {
      assert!(a_index < b_index);

      let (left, right) = self.angular_objects.split_at_mut(b_index);

      let a = &mut left[a_index];
      let b = &right[0];

      // Maintain intertia after collision.
      let inertia =
        a.velocity * a.moment_of_inertia_origin + b.velocity * b.moment_of_inertia_origin;

      let total_area = a.radius * a.radius + b.radius * b.radius;
      a.radius = total_area.sqrt();

      a.mass += b.mass;
      a.calculate_moment_of_inertia();

      // Keep the angle and distance around the middle of the two objects, biased
      // towards the heavier object.
      a.angle = a.angle + (b.angle - a.angle) * b.mass / a.mass;
      a.distance = a.distance + (b.distance - a.distance) * b.mass / a.mass;

      a.velocity = inertia / a.moment_of_inertia_origin;

      self.angular_objects.remove(b_index);
    }

    // Collide angular and linear objects.
    let mut intersection = None;
    'outer: for (i, object) in self.angular_objects.iter().enumerate() {
      for (j, other) in self.linear_objects.iter().enumerate() {
        let distance = (object.position() - other.pos).length();
        if distance < object.radius + 1.0 {
          intersection = Some((i, j));
          break 'outer;
        }
      }
    }

    // Work backward to avoid breaking indexes
    if let Some((a_index, b_index)) = intersection {
      let angular = &mut self.angular_objects[a_index];
      let linear = &mut self.linear_objects[b_index];

      // Transfer all the velocity to the linear object.
      let velocity = angular.velocity * angular.distance;

      // Assume initial linear velocity is zero.
      // e = (v_b - v_a) / velocity;
      //
      // angular.mass * velocity = angular.mass * v_a + linear.mass * v_b;

      let e = 0.355;

      let new_angular_velocity = ((angular.mass / linear.mass) * velocity - e * velocity)
        / (1.0 + angular.mass / linear.mass);
      let new_linear_velocity = new_angular_velocity + velocity / e;

      angular.velocity = new_angular_velocity / angular.distance;

      linear.velocity = Pos {
        x: new_linear_velocity * angular.angle.cos(),
        y: new_linear_velocity * -angular.angle.sin(),
      };

      println!("after collision: {:?}", linear.velocity);
    }

    for object in &mut self.angular_objects {
      // Each object has a downward force of gravity acting on it.
      let force = object.mass * 9.81;
      // That downwards force applies a moment to the object.
      let moment = force * object.distance * object.angle.cos();

      // The angular acceleration is the moment divided by the moment of inertia.
      object.velocity += moment / object.moment_of_inertia_origin * dt;
      object.angle += object.velocity * dt;
    }

    for object in &mut self.linear_objects {
      // Stationary objects stay still.
      if object.velocity.x == 0.0 {
        continue;
      }

      // Each object has a downward force of gravity acting on it.
      let force = object.mass * 9.81;
      // That downwards force applies an acceleration to the object.
      object.velocity.y += force / object.mass * dt;
      object.pos += object.velocity * dt;
    }
  }

  pub fn mouse_to_pos(&self, x: i32, y: i32) -> Pos {
    Pos { x: x as f64 / 50.0 - self.origin.x, y: y as f64 / 50.0 - self.origin.y }
  }
}
