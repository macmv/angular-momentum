use pos::Pos;
use render::{Align, Render};
use sdl2::{event::Event, keyboard::Keycode, mouse::MouseButton, pixels::Color};
use std::{f64::consts::PI, time::Duration};
use world::{LinearObject, World};

mod pos;
mod render;
mod world;

pub fn main() {
  let sdl_context = sdl2::init().unwrap();
  let ttf_context = sdl2::ttf::init().unwrap();

  let video_subsystem = sdl_context.video().unwrap();

  let window = video_subsystem
    .window("rust-sdl2 demo", 1920, 1080)
    .resizable()
    .position_centered()
    .build()
    .unwrap();

  let mut canvas = window.into_canvas().build().unwrap();
  let font = ttf_context.load_font("/usr/share/fonts/TTF/Iosevka-Medium.ttc", 32).unwrap();

  let mut world = World::new();
  let mut paused = true;

  // world.add_linear(LinearObject::new(Pos { x: -8.0, y: 7.0 }, 10.0));

  canvas.set_draw_color(Color::RGB(0, 255, 255));
  canvas.clear();
  canvas.present();
  let mut event_pump = sdl_context.event_pump().unwrap();
  'running: loop {
    canvas.set_draw_color(Color::RGB(80, 80, 80));
    canvas.clear();
    for event in event_pump.poll_iter() {
      match event {
        Event::Quit { .. } | Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
          break 'running
        }
        Event::MouseButtonDown { mouse_btn: MouseButton::Left, x, y, .. } => {
          let pos = world.mouse_to_pos(x, y);
          world.add_angular(world::AngularObject::new_sphere(pos, 1.0));
        }
        Event::MouseButtonDown { mouse_btn: MouseButton::Right, x, y, .. } => {
          let pos = world.mouse_to_pos(x, y);
          world.angular_objects.retain(|object| (object.position() - pos).length() > object.radius);
        }
        Event::KeyDown { keycode: Some(Keycode::Space), .. } => {
          paused = !paused;
        }
        _ => {}
      }
    }
    // The rest of the game loop goes here...

    if !paused {
      world.update(1.0 / 60.0);
    }
    render_world(&mut Render { world: &world, font: &font, canvas: &mut canvas });

    canvas.present();
    ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
  }
}

fn render_world(render: &mut Render) {
  const SCALE: f64 = 50.0;

  const WHITE: Color = Color::RGB(255, 255, 255);
  const GRAY: Color = Color::RGB(180, 180, 180);

  render.draw_text(Pos { x: 0.1, y: -0.8 }, Align::Left, "O", 1.0, WHITE);

  let (window_width, window_height) = render.canvas.window().size();
  let (window_width, window_height) = (window_width as i32, window_height as i32);

  // Draw the axis.
  render.draw_line(
    Pos { x: -render.world.origin.x, y: 0.0 },
    Pos { x: -render.world.origin.x + window_width as f64 / SCALE, y: 0.0 },
  );
  render.draw_line(
    Pos { x: 0.0, y: -render.world.origin.y },
    Pos { x: 0.0, y: -render.world.origin.y + window_height as f64 / SCALE },
  );

  // Draw the tick marks.
  for x in -100..=100 {
    let x = x as f64;
    render.draw_line(Pos { x, y: -0.1 }, Pos { x, y: 0.1 });

    if x != 0.0 {
      render.draw_text(Pos { x, y: -0.6 }, Align::Center, format!("{}", x).as_str(), 0.6, GRAY);
    }
  }

  for y in -100..=100 {
    let y = y as f64;
    render.draw_line(Pos { x: -0.1, y }, Pos { x: 0.1, y });

    if y != 0.0 {
      render.draw_text(
        Pos { x: 0.4, y: y - 0.2 },
        Align::Left,
        format!("{}", y).as_str(),
        0.6,
        GRAY,
      );
    }
  }

  for object in &render.world.angular_objects {
    let pos = object.position();

    render.fill_circle(pos, object.radius);
    render.draw_line(Pos::ZERO, pos);

    let below_object = pos + Pos { x: 0.0, y: object.radius + 0.1 };
    let right_object = pos + Pos { x: object.radius + 0.2, y: -object.radius - 0.2 };

    render.draw_text(
      below_object,
      Align::Center,
      format!("m = {:.2} kg", object.mass).as_str(),
      1.0,
      WHITE,
    );
    render.draw_text(
      right_object,
      Align::Left,
      format!("I = {:.2} kg m^2", object.moment_of_inertia).as_str(),
      1.0,
      WHITE,
    );
    render.draw_text(
      right_object + Pos { x: 0.0, y: 0.8 },
      Align::Left,
      format!("I_o = {:.2} kg m^2", object.moment_of_inertia_origin).as_str(),
      1.0,
      WHITE,
    );

    let (start, end) =
      if object.angle > PI / 2.0 { (PI / 2.0, object.angle) } else { (object.angle, PI / 2.0) };

    render.draw_arc(Pos::ZERO, 2.0, start, end);

    let edge = Pos { x: 2.2 * object.angle.cos() + 0.1, y: 2.2 * object.angle.sin() - 0.3 };

    let align = if object.angle.to_degrees() > 90.0 { Align::Right } else { Align::Left };

    render.draw_text(
      edge,
      align,
      format!("θ = {:.2}°", 90.0 - object.angle.to_degrees()).as_str(),
      1.0,
      WHITE,
    );
    render.draw_text(
      edge + Pos { x: 0.0, y: 0.8 },
      align,
      format!("ω = {:.2} rad/s", object.velocity).as_str(),
      1.0,
      WHITE,
    );
  }

  for object in &render.world.linear_objects {
    let pos = object.pos;

    let radius = 1.0;

    render.fill_circle(pos, radius);

    let below_object = pos + Pos { x: 0.0, y: radius + 0.1 };
    let right_object = pos + Pos { x: radius + 0.2, y: -radius - 0.2 };

    render.draw_text(
      below_object,
      Align::Center,
      format!("m = {:.2} kg", object.mass).as_str(),
      1.0,
      WHITE,
    );

    /*
    render.draw_text(
      right_object,
      align,
      format!("θ = {:.2}°", 90.0 - object.angle.to_degrees()).as_str(),
      1.0,
      WHITE,
    );
    */
    render.draw_text(
      right_object + Pos { x: 0.0, y: 0.8 },
      Align::Left,
      format!("v = {:.2} m/s", object.velocity.x).as_str(),
      1.0,
      WHITE,
    );
  }
}
