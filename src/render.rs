use sdl2::{
  gfx::primitives::DrawRenderer,
  pixels::Color,
  rect::Rect,
  render::{Canvas, Texture},
};

use crate::{pos::Pos, world::World};

pub struct Render<'a, 'b> {
  pub world:  &'a World,
  pub canvas: &'a mut Canvas<sdl2::video::Window>,
  pub font:   &'a sdl2::ttf::Font<'b, 'b>,
}

#[derive(Clone, Copy)]
pub enum Align {
  Left,
  Center,
  Right,
}

const SCALE: f64 = 50.0;

impl Render<'_, '_> {
  fn to_screen(&self, pos: Pos) -> (i32, i32) {
    let x = ((self.world.origin + pos).x * SCALE) as i32;
    let y = ((self.world.origin + pos).y * SCALE) as i32;
    (x, y)
  }

  pub fn fill_circle(&mut self, pos: Pos, radius: f64) {
    let (x, y) = self.to_screen(pos);
    let (x, y) = (x as i16, y as i16);
    let radius = (radius * SCALE) as i16;

    self.canvas.filled_circle(x, y, radius, Color::RGB(255, 255, 255)).unwrap();
  }

  pub fn draw_line(&mut self, start: Pos, end: Pos) {
    let (x1, y1) = self.to_screen(start);
    let (x2, y2) = self.to_screen(end);

    self.canvas.set_draw_color(Color::RGB(255, 255, 255));
    self.canvas.draw_line((x1, y1), (x2, y2)).unwrap();
  }

  pub fn draw_arc(&mut self, pos: Pos, radius: f64, start: f64, end: f64) {
    let (x, y) = self.to_screen(pos);
    let (x, y) = (x as i16, y as i16);
    let radius = (radius * SCALE) as i16;

    self
      .canvas
      .arc(
        x,
        y,
        radius,
        start.to_degrees() as i16,
        end.to_degrees() as i16,
        Color::RGB(255, 255, 255),
      )
      .unwrap();
  }

  pub fn draw_text(&mut self, pos: Pos, align: Align, text: &str, scale: f64, color: Color) {
    let (x, y) = self.to_screen(pos);

    let surface = self.font.render(text).blended(color).unwrap();
    let (width, height) = surface.size();
    let (width, height) = (width as f64 * scale, height as f64 * scale);

    let rect = Rect::new(
      match align {
        Align::Left => x,
        Align::Right => x - width as i32,
        Align::Center => x - width as i32 / 2,
      },
      y,
      width as u32,
      height as u32,
    );

    let texture_creator = self.canvas.texture_creator();
    let texture = Texture::from_surface(&surface, &texture_creator).unwrap();

    self.canvas.copy(&texture, None, Some(rect)).unwrap();
  }
}
