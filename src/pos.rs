use std::ops::{Add, AddAssign, Mul, Sub, SubAssign};

#[derive(Debug, Clone, Copy)]
pub struct Pos {
  pub x: f64,
  pub y: f64,
}

impl Pos {
  pub const ZERO: Pos = Pos { x: 0.0, y: 0.0 };

  pub fn length(&self) -> f64 { (self.x * self.x + self.y * self.y).sqrt() }
}

impl Add for Pos {
  type Output = Pos;

  fn add(self, other: Pos) -> Pos { Pos { x: self.x + other.x, y: self.y + other.y } }
}

impl Sub for Pos {
  type Output = Pos;

  fn sub(self, other: Pos) -> Pos { Pos { x: self.x - other.x, y: self.y - other.y } }
}

impl Mul<f64> for Pos {
  type Output = Pos;

  fn mul(self, scalar: f64) -> Pos { Pos { x: self.x * scalar, y: self.y * scalar } }
}

impl AddAssign for Pos {
  fn add_assign(&mut self, other: Pos) {
    self.x += other.x;
    self.y += other.y;
  }
}

impl SubAssign for Pos {
  fn sub_assign(&mut self, other: Pos) {
    self.x -= other.x;
    self.y -= other.y;
  }
}
